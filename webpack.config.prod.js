var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HTMLWebpackPlugin = require('html-webpack-plugin');

var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry:[
        './src/styles/global.scss',
        './src/js/index.jsx'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015']
                },
                include: path.join(__dirname, 'src/js')
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css-loader!sass-loader'),
                include: path.join(__dirname, 'src/styles')
            },
            //loader for images
            {
                test: /\.(jpg|png)$/,
                loader: 'url-loader?limit=25000',
                include: path.join(__dirname, 'src/assets/images')
            },
            {
                test: /\.svg$/,
                loader: 'file-loader',
                include: path.join(__dirname, 'src/assets/images')
            },
            //loader for icons
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff" },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            },
            //loader for video
            {
                test: /\.mp4$/,
                loader: 'url?limit=10000&mimetype=video/mp4'
            }
        ]
    },
    postcss: [
        require('autoprefixer')
    ],
    plugins: [
        new HTMLWebpackPlugin({
            hash:true,
            title: 'tv4e',
            filename: 'index.html',
            template: 'index.html',
            environment: process.env.NODE_ENV
        }),
        new ExtractTextPlugin("style.css"),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false
            }
        }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.DedupePlugin()
    ],
    sassLoader: {
        includePaths: [path.join(__dirname, 'src/style')]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    }
};