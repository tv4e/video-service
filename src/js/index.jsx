'use strict';

import React from 'react';
import ReactDom from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import { Provider } from 'react-redux';
import Store from'./store/Store';

import Root from './containers/Root';
import NotFound from './components/notfound/NotFound';

import Welcome from './containers/Welcome';
import VideoService from './containers/VideoService';

import Auth from './auth';

require('../styles/global.scss');

export const store = Store();

store.subscribe(()=>{
    if(store.getState().loginSuccess.access_token){
        if (Auth.loggedIn()) {
            browserHistory.replace({
                pathname: '/dash',
            });
        }
    }

    if(store.getState().logoutSuccess.status == 200){
        if (!Auth.loggedIn()) {
            browserHistory.replace({
                pathname: '/',
            });
        }
    }

    if(store.getState().errorCode == 401 || store.getState().errorCode == 500){
        Auth.logout();
        browserHistory.replace({
            pathname: '/',
        });
    }

});

ReactDom.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route onEnter={_checkAuth} path="/" component={Welcome} />

            <Route onEnter={_requireAuth} path="/dash" component={Root}>
                <IndexRoute component={VideoService}/>
            </Route>

            <Route path="*" component={NotFound} />
        </Router>
    </Provider>,
    document.getElementById('root')
);

function _requireAuth() {
    console.log(Auth.loggedIn());

    if (!Auth.loggedIn()) {
        browserHistory.push('/')
    }
}

function _checkAuth() {
    if (Auth.loggedIn()) {
        browserHistory.push('/dash')
    }
}