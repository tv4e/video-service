'use strict';

import { createStore, combineReducers, applyMiddleware } from 'redux';

import * as allReducers from './../reducers/reducers';

import thunk from 'redux-thunk';

// Combine Reducers
const reducers = combineReducers(allReducers);

export default function Store(initialState) {
    return createStore(
        reducers,
        initialState,
        applyMiddleware(thunk)
    );
}