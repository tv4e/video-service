'use strict';

module.exports = {

    getToken() {
        return localStorage.token
    },

    setToken(token) {
        localStorage.token = token;
    },

    logout() {
        delete localStorage.token;
    },

    loggedIn() {
        return !!localStorage.token
    },

};