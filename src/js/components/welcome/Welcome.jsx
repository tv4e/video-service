'use strict';

import React from 'react';
import Form from '../../containers/FormRedux';

export default class Welcome extends React.Component {

    render() {
        return (
            <section className="welcome">
                <div className="welcome__box">
                    <img src="../../../assets/images/logo/logo.png" alt="logo"/>

                    <Form id="welcome" component="Input" placeholder="John Doe" name="username" label="Username"/>
                    <Form id="welcome" component="Input" placeholder="******" type="password" name="password" label="Password"/>
                    <Form id="welcome" onSubmit={this._onSubmit.bind(this)} component="Submit"/>

                </div>
            </section>
        );
    };

    _onSubmit(values){
        this.props.actionCalls.login(values);
    }

};
