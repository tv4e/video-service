'use strict';

import React from 'react';
import {Icon} from 'react-foundation';

export default class SelectImage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isSelected: this.props.isSelected
        };
    };

    render() {
        let isSelected = !this.state.isSelected ? 'selectimage--image' : 'selectimage--image selectimage--image-selected';

        return(
            <section className="selectimage">
                <Icon onClick={this.props.onClear.bind(this,this.props.src)} name="fi-x"/>
                <div onClick={this._selectImage.bind(this)} className={isSelected}>
                    <img src= {this.props.src} alt={this.props.alt}/>
                </div>
            </section>
        );
    };

    _selectImage(){
        this.setState({isSelected:!this.state.isSelected});
    }

};

SelectImage.propTypes = {
    isSelected:React.PropTypes.bool,
    src:React.PropTypes.string,
    alt:React.PropTypes.string
};

SelectImage.defaultProps = {
    isSelected:false,
};

