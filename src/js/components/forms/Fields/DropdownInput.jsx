'use strict';

import React from 'react';
import Select from 'react-select';
import _ from 'lodash';

export default class DropdownInput extends React.Component {
    constructor(props) {
        super(props);

        this.backspace = false;
        this.inputText = '';
        this.receivedProp = '';

        this.state = {
            options:this.props.options,
            noActionCloneOptions: null,
            value:null,
            isLoading: this.props.isLoading,
            noResultsText:true
        };

    }

    componentWillReceiveProps(nextProps){
        this.state.options=nextProps.options;
        this.state.isLoading=nextProps.isLoading;
    }

    shouldComponentUpdate(nextProps, nextState){
        this.state.value= nextState.value;

        if(nextProps.value != this.receivedProp){
            this.receivedProp = nextProps.value;
            this.state.value= nextProps.value;

            if(!_.isEmpty(nextProps.value)){
                this.props.actionCalls.form(this.props.id, this.props.name, nextProps.value.label);
            }else{
                this.props.actionCalls.form(this.props.id, this.props.name, '');
            }
        }

        return true;
    }

    render() {
        let noResultsClass = this.props.noResults? ' noResults__Dropdown ' : '';
        let noResultsClassHide = this.state.noResultsText===false? ' noResults__Dropdown-hide ' : '';
        return (
            <div className={noResultsClass+noResultsClassHide}>
                {this.props.label?<label>{this.props.label}</label>:null}
                <Select
                    name={this.props.name}
                    value={this.state.value}
                    options={this.state.options}
                    onChange={this._handleChange.bind(this)}
                    isLoading={this.state.isLoading}
                    onInputKeyDown={this._handleKey.bind(this)}
                    onInputChange = {this._onInputChange.bind(this)}
                    noResultsText= {this.props.noResultsText}
                    onBlurResetsInput= {false}
                    clearable={this.props.clearable}
                />
            </div>
        );
    };

    _handleChange(value){
        if(!_.isEmpty(value)){
            this.props.actionCalls.form(this.props.id, this.props.name, value.label);
        }else{
            value = null;
            this._handleKey('del');
        }

        this.setState({value});

        if(this.props.onChange){this.props.onChange(value);}
    }

    _onInputChange(event){

        if(this.props.noResultsText === false){
            let includes = true;

            _.forEach(this.state.options, option=>{
                if(!(option.label.toLowerCase().includes(event.toLowerCase()))){
                    includes = false;
                    return false;
                }
            });

            if(!includes || _.isEmpty(this.state.options)){
                this.setState({noResultsText:false});
            }else{
                this.setState({noResultsText:true});
            }
        }

        this.inputText = event;
        this.props.actionCalls.form(this.props.id, this.props.name, event);
    }

    _handleKey(e){
        if(e != 'del'){
            switch (e.keyCode){
                case 13:
                    let clonedOptions = _.cloneDeep(this.state.options);
                    let includes = false;

                    _.forEach(clonedOptions, option=>{
                        if(!(option.label.toLowerCase().includes(this.inputText.toLowerCase()))){
                            includes = true;
                            return false;
                        }
                    });

                    if(includes || _.isEmpty(clonedOptions)){
                        if(this.props.noResults){
                            this.props.noResults(this.inputText);
                        }
                    }
                    break;
                case 8:
                    this.backspace = true;
                    return null;
            }
        }else if (!this.backspace){
            this.props.onClear();
        }
        this.backspace = false;
    }



};

DropdownInput.propTypes = {
    options: React.PropTypes.array,
    name: React.PropTypes.string.isRequired,
    label: React.PropTypes.string,
    value: React.PropTypes.object,
    isLoading: React.PropTypes.bool,
    onChange: React.PropTypes.func,
    onClear: React.PropTypes.func,
    clearable: React.PropTypes.bool,
    noResults: React.PropTypes.func
};

DropdownInput.defaultProps = {
    isLoading: false,
};

