'use strict';

import React from 'react';
import update from 'react-addons-update'; // ES6

import _ from 'lodash';
import {Row, Column, Icon, Thumbnail} from 'react-foundation';
import {Input} from '../FormRedux';

export default class AddInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inputRow:[],
            flagContent:false
        };
    };

    componentWillReceiveProps(){
        this.state.flagContent = false;
    };

    render() {

        let inputRowHTML = [];

        _.forEach(this.state.inputRow,function(value,key){
            _.forEach(value,function(value2,key2){
                if(key2==='HTML'){
                    inputRowHTML.push(value2);
                }
            });
        });

        return (
            <div>
                <div className="title">Content</div>
                <Row className="input__list--element">
                    <Column large={5}>
                        <Input name="value" label="Element" ref="inputElement" placeholder="div.post-info-bar > h1.page-title" />
                    </Column>
                    <Column large={5}>
                        <Input name="key" label="content" ref="inputContent" placeholder="div.post-info-bar > h1.page-title" />
                    </Column>
                    <Column large={2}>
                        <label className="invisible"> Invisible</label>
                            <div>
                                <button type="button" onClick={this._addObject.bind(this)}><Icon name="fi-plus"/></button>
                            </div>
                    </Column>
                </Row>

                {inputRowHTML}

            </div>
        );
    };

    componentDidUpdate(){
        if (this.props.content && !this.state.flagContent){
            this.setState({flagContent:true},function () {
                this._setContent();
            });
        }
    };

    _setContent(){
        let setRows = [];

        _.forEach(this.props.content,function(v,k){
            let newRow = {};

            newRow.value1 = k;
            newRow.value2 = v;

            if(!newRow.value1 || !newRow.value2)
                return null;

            newRow.HTML = this._getElementHTML(newRow.value1, newRow.value2, setRows.length);

            setRows.push(newRow);
        }.bind(this));

        this.setState({
            inputRow: setRows
        });
    };

    _addObject() {
        let inputRow = this.state.inputRow;
        let newRow = {};

        newRow.value1 = this.refs.inputElement.state.value;
        newRow.value2 = this.refs.inputContent.state.value;

        if(!newRow.value1 || !newRow.value2)
            return null;

        newRow.HTML = this._getElementHTML(newRow.value1, newRow.value2);

        this.setState({
            inputRow: update(inputRow, {$push: [newRow]}),
        }, function(){
            this.refs.inputElement._clearValue();
            this.refs.inputContent._clearValue();
        });
    };

    _deleteObject(index){
        let inputRow = this.state.inputRow;
        inputRow = update(inputRow, {$splice: [[index,1]]});

        this.setState({inputRow: inputRow},function(){this._reIndex()});
    };

    _reIndex(){
        let inputRow = this.state.inputRow;
        _.forEach(inputRow, function(value, key) {
            inputRow[key].value1 = value.value1;
            inputRow[key].value2= value.value2;
            inputRow[key].HTML = this._getElementHTML(value.value1, value.value2, key);
        }.bind(this));

        this.setState({inputRow: []},function(){
            this.setState({inputRow: inputRow});
        });
    };

    _getElementHTML(v1,v2, length){
        let inputRow = this.state.inputRow;
        let _length = length === undefined?inputRow.length:length;

        let elementHTML=(
            <Row key={_length} className="input__list--element">
                <Column large={5}>
                    <Input name={"key"+_length} type="text" value={v1} placeholder="div.post-info-bar > h1.page-title" />
                </Column>
                <Column large={5}>
                    <Input name={"value"+_length} type="text" value={v2} placeholder="div.post-info-bar > h1.page-title" />
                </Column>
                <Column large={2}>
                    <button type="button" onClick={this._deleteObject.bind(this, _length)}><Icon name="fi-minus"/></button>
                </Column>
            </Row>
        );

        return elementHTML;

    };

};

AddInput.propTypes = {
    content: React.PropTypes.object
};

AddInput.defaultProps = {
    content:null
};

