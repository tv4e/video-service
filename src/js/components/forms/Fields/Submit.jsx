'use strict';

import React from 'react';
import {Button} from 'react-foundation';

export default class Submit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };
    }

    render() {

        let body = this.props.children?this.props.children:
            <Button onClick={this._onSubmit.bind(this)} type="submit" >Save</Button>;


        return (
            <div>
                {body}
            </div>
        );
    };

    _onSubmit(){
        this.props.onSubmit(this.props.formData[this.props.id]);
    }

};
