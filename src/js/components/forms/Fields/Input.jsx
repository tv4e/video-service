'use strict';

import React from 'react';

export default class Input extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            value: '',
            stateChange:false
        };
    }

    componentWillReceiveProps(nextProps){
        if(!this.state.stateChange){
            if(this.state.value !=nextProps.value){
                this.props.actionCalls.form(this.props.id, this.props.name, nextProps.value);
            }
            this.state.value = nextProps.value;
        }
        this.state.change = false;
    }

    render() {
        return (
            <div>
                {this.props.label?<label>{this.props.label}</label>:null}
                <input
                    type={this.props.type}
                    label={this.props.label}
                    value={this.state.value}
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    onChange={this._handleInputChange.bind(this)}
                />
            </div>
        );

    }

    _handleInputChange(event){
        this.props.actionCalls.form(this.props.id, this.props.name, event.target.value);
        this.setState({value:event.target.value, stateChange:true});
    }

    _clearValue(){
        this.setState({value:'', stateChange:true});
    }

};

Input.propTypes = {
    value: React.PropTypes.string,
    name: React.PropTypes.string.isRequired,
    type: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    label: React.PropTypes.string,
    id: React.PropTypes.string
};

Input.defaultProps = {
    type:'text'
};

