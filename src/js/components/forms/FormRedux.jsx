'use strict';

import React from 'react';
import _ from 'lodash';

import DropdownInput from './Fields/DropdownInput';
import Input from './Fields/Input';
import Submit from './Fields/Submit';

export default class FormRedux extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            children:null,
            values:{}
        };
    }

    render() {
        const Components = {
            DropdownInput,
            Input,
            Submit
        };

        let Field = Components[this.props.component];

        return (
            <div>
                <Field {...this.props} />
            </div>
        );
    };

}

FormRedux.propTypes = {
    component: React.PropTypes.string,

};

FormRedux.defaultProps = {
    component: '',
};
