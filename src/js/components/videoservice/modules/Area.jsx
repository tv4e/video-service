'use strict';

import React from 'react';

import Form from '../../../containers/FormRedux';

import Dropzone from 'react-dropzone';

import {Row, Column, Thumbnail} from 'react-foundation';
import SelectImage from '../../tools/SelectImage.jsx';

import _ from 'lodash';

export default class Area extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            asgies: [],
            asgie: null,
            uploadedImages: [],
            imageAreaShow:false,
            receivedProp:''
        };
    };

    //use component did update to check props and change them if needed
    componentWillReceiveProps(nextProps){
        if(!_.isEmpty(nextProps.asgie)){
            this.state.imageAreaShow = true;
        }

        let items = _.isEmpty(nextProps.actionData.asgies.data) ? null : nextProps.actionData.asgies.data;
        this._setOptions(items);

        if(!_.isEmpty(nextProps.asgie) && nextProps.asgie != this.state.receivedProp){
            this.state.receivedProp = nextProps.asgie;
            this.state.asgie= nextProps.asgie;
        }
    };

    shouldComponentUpdate(nextProps,nextState){
        this.props = nextProps;
        this.state.asgie = nextState.asgie;

        return true;
    };

    render() {
        let resources = this._setImages(this.props.resources);
        return (
            <div>
                <div className="title">Area</div>
                <Row>
                    <Column className="input__list" small={12}  large={6} >
                        <div className="input__list--element">
                            <Form
                                id="services"
                                component="DropdownInput"
                                name="ASGIE"
                                label="ASGIE"
                                value={this.state.asgie}
                                options={this.state.asgies}
                                onChange={this._onChangeAsgie.bind(this)}
                                clearable={false}
                            />
                        </div>

                        <div className="input__list--element">
                            <Form
                                id="services"
                                component="DropdownInput"
                                name="Music"
                                label="Music"
                                value={resources.audio.selected}
                                options={resources.audio}
                            />
                        </div>
                    </Column>

                    {this.state.imageAreaShow &&
                        <Column small={12}  large={6} >
                            <Row upOnSmall={2} upOnMedium={2} upOnLarge={4}>

                                <Column>
                                    <Dropzone className="input__list--image"
                                              onDrop={this._setUploadedImages.bind(this)}>
                                        <img src="../../../../assets/images/upload.png" alt="dropImage" />
                                    </Dropzone>
                                </Column>

                                {this.state.uploadedImages}
                                {resources.images}
                            </Row>
                        </Column>
                    }


                </Row>
            </div>
        );
    };

    _setOptions(data){
        let asgies = [];
        _.forEach(data, function(value,key){
            asgies.push({value: value.id, label: value.title});
        });

        this.setState({asgies: asgies, isLoading:false});
    };

    _onChangeAsgie(data){
        if(!_.isEmpty(data)){
            this.props.actionCalls.asgie(data.value);
            this.setState({imageAreaShow:true, asgie:data});
        }else{
            this.setState({imageAreaShow:false});
        }
    }

    _setUploadedImages(files){
        let uploadedImages = _.cloneDeep(this.state.uploadedImages);

        _.forEach(files, function(v,k){
            let data = {image:v, asgie:this.state.asgie.value, selected:0};
            this.props.actionCalls.resourcesPost(data);
            uploadedImages.push(
                <Column key={'uploaded_image'+ uploadedImages.length}>
                    <SelectImage onClear={this._clearImage.bind(this)} src={v.preview} alt={'uploaded_image'+ uploadedImages.length}/>
                </Column>
            );
        }.bind(this));

        this.setState({uploadedImages});
    }

    _setImages(data){
        let resources = {};
        resources.images = [];
        resources.audio = [];

        if(data !=null){
            _.forEach(data.image, function(v,k){
                let isSelected = false;

                if(v.selected){
                    isSelected = true;
                }

                resources.images.push(
                    <Column key={`image${k}`}>
                        <SelectImage  onClear={this._clearImage.bind(this)} src={v.url} alt="" isSelected={isSelected}/>
                    </Column>
                );
                //
                // if(v.type==='audio'){
                //     if(v.selected){
                //         resources.audio.selected={value: k, label: v.name};
                //     }
                //     resources.audio.push({value: k, label: v.name});
                // }
            }.bind(this));
        }

        return resources;
    };

    _clearImage(path){
        this.props.actionCalls.deleteResource(path,this.state.asgie);
    }

};

Area.propTypes = {
    asgie:React.PropTypes.object,
};

Area.defaultProps = {
    asgie:{},
};

