'use strict';

import React from 'react';
import {ModalContainer, ModalDialog} from 'react-modal-dialog';
import {Button} from 'react-foundation';

import Form from '../../../containers/FormRedux';

import _ from 'lodash';

export default class URL extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            options:[],
            isLoading: false,
            value:null,
            isShowingModal: false,
            valueToDelete: null
        };
    }

    //use component did update to check props and change them if needed
    componentWillReceiveProps(nextProps){
        let items = _.isEmpty(nextProps.actionData.data) ? null : nextProps.actionData.data;

        this._setOptions(items);
    };

    render() {
        return (
            <div>
                <div className="title">Service</div>
                    <Form
                        id="services"
                        component="DropdownInput"
                        name="URL"
                        label="URL"
                        value={this.state.value}
                        onChange={this._setValue.bind(this)}
                        onClear={this._handleClear.bind(this)}
                        options={this.state.options}
                        isLoading={this.state.isLoading}
                        noResultsText={false}
                    />

                    {this.state.isShowingModal &&
                        <ModalContainer onClose={this._handleClose.bind(this)}>
                            <ModalDialog onClose={this._handleClose.bind(this)}>
                                <h1>Are you sure you want to delete</h1>
                                <Button onClick={this._onDelete.bind(this)} type="submit" >Yes</Button>
                                <Button onClick={this._handleClose.bind(this)} type="submit" >Cancel</Button>
                            </ModalDialog>
                        </ModalContainer>
                    }
            </div>
        );

    };

    _setOptions(items){
        let options = [];

        _.forEach(items, function(value,key){
            options.push({value: value.id, label: value.url, key: key});
        });

        this.setState({options: options, isLoading:false});
    };

    _setValue(value){
        this.setState({value, valueToDelete:this.state.value}, function(){
            this.props.onChange(value);
        }.bind(this));
    }

    _handleClear(){this.setState({isShowingModal: true})}
    _handleClose(){this.setState({isShowingModal: false})}

    _onDelete(){
       let id = this.state.valueToDelete.value;
        this.props.actionCalls.deleteSource(id);
    }
};


URL.propTypes = {
    onChange: React.PropTypes.func
};

URL.defaultProps = {
    onChange: null
};
