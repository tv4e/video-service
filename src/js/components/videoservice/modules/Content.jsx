'use strict';

import React from 'react';

import {Row, Column, Icon, Thumbnail} from 'react-foundation';

import Form from '../../../containers/FormRedux';

export default class Content extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            content: '',
            title: '',
            description: ''
        };
    }

    shouldComponentUpdate(nextProps){
        this.props = nextProps;
        this.state.content = nextProps.content;
        this.state.title = nextProps.title;
        this.state.description = nextProps.description;
        return true;
    }

    render() {
        return (
            <div>
                <div className="title">Content</div>

                <div className="input__list--element">
                    <Form id="services" component="Input" name="ContentContainer" label="Container" type="text" value={this.state.content} placeholder="div.post-info-bar > h1.page-title" />
                </div>

                <div className="input__list--element">
                    <Form id="services" component="Input" name="Title" label="Title" type="text" value={this.state.title} placeholder="div.post-info-bar > h1.page-title" />
                </div>

                <div className="input__list--element">
                    <Form id="services" component="Input" name="Description" label="Description"  type="text" value={this.state.description} placeholder="div.post-info-bar > h1.page-title" />
                </div>

            </div>
        );
    };

};

Content.propTypes = {
    title: React.PropTypes.string,
    description: React.PropTypes.string
};

Content.defaultProps = {
    title:'',
    description:''
};

