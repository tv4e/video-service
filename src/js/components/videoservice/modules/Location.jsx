'use strict';

import React from 'react';

import {Row, Column, Icon, Thumbnail} from 'react-foundation';

import Form from '../../../containers/FormRedux';
import _ from 'lodash';

export default class Location extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            districts: [],
            district: null,
            cities: [],
            city: null
        };
    }

    //use component did update to check props and change them if needed
    componentWillReceiveProps(nextProps){
        let items = _.isEmpty(nextProps.actionData.data) ? null : nextProps.actionData.data;
        this._setOptions(items);

        if(!_.isEmpty(nextProps.district) && nextProps.district != this.state.receivedProp){
            this.state.receivedProp = nextProps.asgie;
            this.state.district= nextProps.district;
        }
    };

    shouldComponentUpdate(nextProps){
        this.props = nextProps;
        this.state.district = nextProps.district;
        return true;
    }

    render() {
        return (
            <div>
                <div className="title">Location</div>

                <div className="input__list--element">
                    <Row upOnSmall={2} upOnMedium={2} upOnLarge={2}>
                        <Column>
                            <Form
                                id="services"
                                component="DropdownInput"
                                value={this.state.district}
                                options={this.state.districts}
                                clearable={false}
                                onChange={this._onChangeDistrict.bind(this)}
                                name="District"
                                label="District"
                            />
                        </Column>

                        <Column>
                            <Form
                                id="services"
                                component="DropdownInput"
                                value={this.state.city}
                                options={this.state.cities}
                                clearable={false}
                                name="City"
                                label="City"
                            />
                        </Column>
                    </Row>
                </div>
            </div>
        );
    };


    _setOptions(data){
        let districts = [];

        districts.push({value: 'Portugal', label: 'Portugal'});

        _.forEach(data, function(value,key){
            districts.push({value: value.id, label: value.name, key:key});
        });


        this.setState({districts, isLoading:false});
    };

    _onChangeDistrict(data){

        if(_.isEmpty(data)){
            this.setState({cities:[], city:null});
            return;
        }

        let cities=[];
        let city={value: 'All', label: 'All'};

        if(data.key === undefined){
            cities.push(city);
            this.setState({city});
        }else{
            let districts = this.props.actionData.data;
            cities.push(city);
            city=null;

            _.forEach(districts[data.key].cities, function(value,key){
                cities.push({value: value.name, label: value.name});
            });

        }

        this.setState({cities, city});

    }

};

Location.propTypes = {
    district: React.PropTypes.string,
};

Location.defaultProps = {
    district:'',
};

