'use strict';

import React from 'react';

import {Row, Column, Icon, Thumbnail} from 'react-foundation';

import Form from '../../../containers/FormRedux';

export default class NewsList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newsLink: '',
            newsContainer: ''
        };
    }

    shouldComponentUpdate(nextProps){
        this.props = nextProps;
        this.state.newsContainer = nextProps.newsContainer;
        this.state.newsLink = nextProps.newsLink;
        return true;
    }

    render() {
        return (
            <div>
                <div className="title">News List</div>

                <div className="input__list--element">
                    <Form id="services" component="Input" label="News Container" name="NewsContainer" type="text" value={this.state.newsContainer} placeholder="div.post-info-bar > h1.page-title" />
                </div>

                <div className="input__list--element">
                    <Form id="services" component="Input" label="News Link" name="NewsLink" type="text" value={this.state.newsLink} placeholder="div.post-info-bar > h1.page-title" />
                </div>
            </div>
        );
    };

};

NewsList.propTypes = {
    newsContainer: React.PropTypes.string,
    newsLink: React.PropTypes.string
};

NewsList.defaultProps = {
    newsContainer:'',
    newsLink:''
};

