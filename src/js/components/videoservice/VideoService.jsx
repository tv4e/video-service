'use strict';

import React from 'react';

import {Row, Column, Icon, Button} from 'react-foundation';

import Form from '../../containers/FormRedux';

import Location from './modules/Location';
import Content from './modules/Content';
import NewsList from './modules/NewsList';
import Area from './../../containers/Area';
import URL from './modules/URL';

export default class VideoService extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            newsContainer:'',
            newsLink:'',
            asgie: null,
            music: null,
            content: '',
            title: '',
            description:'',
            resources:[]
        };
    }

    componentWillMount() {
        this.props.actionCalls.sources();
        this.props.actionCalls.asgies();
        this.props.actionCalls.districts();
    };

    //use component did update to check props and change them if needed
    componentWillReceiveProps(nextProps){
        let resources = _.isEmpty(nextProps.actionData.asgie.data) ? null : nextProps.actionData.asgie.data.resources;
        this._setAsgieImage(resources);
    };

    render() {
        return (
            <section className="videoService">
                <div className="videoService__form">
                    <div className="videoService__form--element">
                        <URL
                            onChange={this._setContent.bind(this)}
                            actionData={this.props.actionData.sources}
                            actionCalls={this.props.actionCalls}
                        />
                    </div>

                    <div className="videoService__form--element">
                        <Area
                            asgie={this.state.asgie}
                            resources={this.state.resources}
                        />
                    </div>


                    <div>
                        <div className="videoService__form--element">
                            <Location
                                actionData={this.props.actionData.districts}
                            />
                        </div>
                        <div className="videoService__form--element">
                            <Row>
                                <Column className="input__list" large={6}>
                                    <NewsList
                                        newsContainer={this.state.newsContainer}
                                        newsLink={this.state.newsLink}
                                    />
                                </Column>

                                <Column className="input__list" small={12} large={6}>
                                    <Content
                                        content={this.state.content}
                                        title={this.state.title}
                                        description={this.state.description}
                                    />
                                </Column>
                            </Row>
                        </div>
                    </div>

                    <Form id="services" onSubmit={this._onSubmit.bind(this)} component="Submit"/>

                </div>
            </section>
        );
    };

    _setContent(data){
        if(!_.isEmpty(data)){

            if(!data.added){
                let source = this.props.actionData.sources.data[data.key];
                this.props.actionCalls.asgie(source.asgie_id);

                let newsContainer= source.newsContainer;
                let newsLink= source.newsLink;
                let asgie = {value:data.value, label:source.rel_asgie.title};

                let content = source.contentContainer;
                let title = source.contentTitle;
                let description = source.contentDescription;

                this.setState({newsContainer, newsLink, asgie, content, title, description});
            }else{
                this.setState({newsContainer:'', newsLink:'', asgie:null, content:'', title:'', description:''});
            }

        }

    }

    _setAsgieImage(resources){
        this.setState({resources});
    }

    _onSubmit(data){
        console.log(data);
        // this.props.actionCalls.postSources(data);
    }
};

VideoService.propTypes = {};

VideoService.defaultProps = {
};



