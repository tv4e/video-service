'use strict';

import React from 'react';
import {TopBar, TopBarTitle, TopBarRight, Row, Column, Menu, MenuItem, Button} from 'react-foundation';

export default class Root extends React.Component {
    render() {

        return (
            <section className="dashboard">
                <section className="navbar">
                    <TopBar className="navbar__topbar">
                        <Row>
                            <Column className="navbar__topbar-title">
                                <TopBarTitle>Video Dashboard</TopBarTitle>
                            </Column>
                            <Column>
                                <TopBarRight className="navbar__right">
                                    <Menu>
                                        <MenuItem>
                                            <Button onClick={this._logout.bind(this)}>Log out</Button>
                                        </MenuItem>
                                    </Menu>
                                </TopBarRight>
                            </Column>
                        </Row>
                    </TopBar>
                </section>

                <section className="body">
                    <Row className="body__content">
                        <Column>
                            <div className="body__content--container">
                                {this.props.children}
                            </div>
                        </Column>
                    </Row>
                </section>

            </section>
        );
    };

    _logout(){
        this.props.actionCalls.logout();
    }

};
