'use strict';

import React from 'react';

import {Row, Column, Icon, Button} from 'react-foundation';

export default class NotFound extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (
            <section className="notFound">
              NOT FOUND
            </section>
        );
    };


};

NotFound.propTypes = {};

NotFound.defaultProps = {
};



