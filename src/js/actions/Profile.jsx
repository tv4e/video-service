'use strict';
import axios from 'axios';
import query_string from 'query-string';
import Auth from '../auth';
import {errors, errorCode} from './Errors';

//ACTION CREATORS
export function loginSuccess(response) {
    return {
        type: 'LOGIN_SUCCESS',
        response
    };
}

export function loginHasErrored(bool) {
    return {
        type: 'LOGIN_HAS_ERRORED',
        hasErrored: bool
    };
}

export function loginIsLoading(bool) {
    return {
        type: 'LOGIN_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function login(data) {

    if(Auth.loggedIn()){
        return null;
    }

    let payload = {
        grant_type: 'password',
        client_id:2,
        client_secret: '2swKu8IxdF2BrTKXJUamnotBuC1dda8c1pcoZOVU',
        username:data.username,
        password:data.password
    };

    return (dispatch) => {
        dispatch(loginIsLoading(true));
        axios({
            method:'post',
            url:'http://api_mysql.tv4e.pt/oauth/token',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept' : 'application/x-www-form-urlencoded'
            },
            data: query_string.stringify(payload)
        })
        .then((response) => {

            if (response.status != 200) {
                console.log('error');
                throw Error(response);
            }

            response = response.data;

            dispatch(loginIsLoading(false));
            Auth.setToken(response.access_token);
            dispatch(logoutSuccess([]));
            dispatch(loginSuccess(response));
        })
        .catch((error) => {
            if (error.response) {
                errors(error.response.status);
            }
            console.log('error', error.statusText);
            dispatch(loginHasErrored(true))
        });
    };
}



//ACTION CREATORS
export function logoutSuccess(response) {
    return {
        type: 'LOGOUT_SUCCESS',
        response
    };
}

export function logoutHasErrored(bool) {
    return {
        type: 'LOGOUT_HAS_ERRORED',
        hasErrored: bool
    };
}

export function logoutIsLoading(bool) {
    return {
        type: 'LOGOUT_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function logout() {
    let token = localStorage.token;

    if(!Auth.loggedIn()){
        return null;
    }

    return (dispatch) => {
        dispatch(logoutIsLoading(true));
        axios({
            method:'get',
            url:'http://api_mysql.tv4e.pt/api/user/logout',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept' : 'application/x-www-form-urlencoded',
                'X-Requested-With' : 'XMLHttpRequest',
                'Authorization' : 'Bearer '  + token
            }
        })
        .then((response) => {
            if (response.status != 200) {
                console.log('error');
                throw Error(response.statusText);
            }

            dispatch(logoutIsLoading(false));
            dispatch(loginSuccess([]));
            Auth.logout();
            dispatch(logoutSuccess(response));
        })
        .catch(() => dispatch(logoutHasErrored(true)));
    };
}