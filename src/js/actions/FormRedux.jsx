'use strict';
//ACTION CREATORS
export function formData(response) {
    return {
        type: 'FORM_DATA',
        response
    };
}

//ACTION CALLS
export function form(id, name,value) {
    return (dispatch) => {
        dispatch(formData({
            [id]:{
                [name]:value
            }
        }));
    }
}

