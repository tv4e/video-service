'use strict';
import axios from 'axios';
import {errors, errorCode} from './Errors';
import query_string from 'query-string';

//ACTION CREATORS
export function sourcesFetchDataSuccess(response) {
    return {
        type: 'SOURCES_FETCH_DATA_SUCCESS',
        response
    };
}

export function sourcesHasErrored(bool) {
    return {
        type: 'SOURCES_HAS_ERRORED',
        hasErrored: bool
    };
}

export function sourcesIsLoading(bool) {
    return {
        type: 'SOURCES_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function sources() {
    let token = localStorage.token;

    return (dispatch) => {
        dispatch(sourcesIsLoading(true));

        axios({
            url: 'http://api_mysql.tv4e.pt/api/sources',
            method:'get',
            headers:{
                'Authorization' : 'Bearer '  + token
            }
        })
        .then((response) => {
            if (response.status != 200) {
                throw Error(response.statusText);
            }

            dispatch(sourcesIsLoading(false));
            dispatch(sourcesFetchDataSuccess(response.data));

            return response;
        })
        .catch((error) => {
            if (error.response) {
                errors(error.response.status);
            }

            dispatch(sourcesHasErrored(true));
        });
    };
}


//....................................................................................

export function asgiesFetchDataSuccess(response) {
    return {
        type: 'ASGIES_FETCH_DATA_SUCCESS',
        response
    };
}

export function asgiesHasErrored(bool) {
    return {
        type: 'ASGIES_HAS_ERRORED',
        hasErrored: bool
    };
}

export function asgiesIsLoading(bool) {
    return {
        type: 'ASGIES_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function asgies() {
    let token = localStorage.token;

    return (dispatch) => {
        dispatch(asgiesIsLoading(true));

        axios({
            url:'http://api_mysql.tv4e.pt/api/asgies',
            method:'get',
            headers:{
                'Authorization' : 'Bearer '  + token
            }
        })
        .then((response) => {
            if (response.status != 200) {
                throw Error(response.statusText);
            }

            dispatch(asgiesIsLoading(false));
            dispatch(asgiesFetchDataSuccess(response.data));
            return response;
        })
        .catch((error) => {
            if (error.response) {
                errors(error.response.status);
            }
            dispatch(asgiesHasErrored(true));
        });
};
}


//....................................................................................

export function districtsFetchDataSuccess(response) {
    return {
        type: 'DISTRICTS_FETCH_DATA_SUCCESS',
        response
    };
}

export function districtsHasErrored(bool) {
    return {
        type: 'DISTRICTS_HAS_ERRORED',
        hasErrored: bool
    };
}

export function districtsIsLoading(bool) {
    return {
        type: 'DISTRICTS_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function districts() {
    let token = localStorage.token;

    return (dispatch) => {
        dispatch(districtsIsLoading(true));

        axios({
            url: 'http://api_mysql.tv4e.pt/api/districts',
            method : 'get',
            headers : {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization' : 'Bearer '  + token
            }
        })
            .then((response) => {
                if (response.status != 200) {
                    throw Error(response);
                }

                dispatch(districtsIsLoading(false));
                dispatch(districtsFetchDataSuccess(response.data));
                return response;
            })
            .catch((error) =>{
                console.log(error);
                if (error.response) {
                    errors(error.response.status);
                }

                dispatch(districtsHasErrored(true));
            });
    };
}

//....................................................................................
//ACTION CREATORS
export function postSourcesSuccess(response) {
    return {
        type: 'POST_SOURCES_SUCCESS',
        response
    };
}

export function postSourcesHasErrored(bool) {
    return {
        type: 'POST_SOURCES_HAS_ERRORED',
        hasErrored: bool
    };
}

export function postSourcesIsLoading(bool) {
    return {
        type: 'POST_SOURCES_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function postSources(data) {
    let token = localStorage.token;

    return (dispatch) => {
        dispatch(postSourcesIsLoading(true));

        axios({
            url: 'http://api_mysql.tv4e.pt/api/sources',
            method:'post',
            headers:{
                'Authorization' : 'Bearer '  + token,
                'X-Requested-With' : 'XMLHttpRequest'
            },
            data : data
        })
            .then((response) => {
                if (response.status != 200) {
                    throw Error(response.statusText);
                }

                dispatch(postSourcesIsLoading(false));
                dispatch(postSourcesSuccess(response.data));
                dispatch(sources());
                return response;
            })
            .catch((error) => {
                if (error.response) {
                    errors(error.response.status);
                }

                dispatch(postSourcesHasErrored(true));
            });
    };
}

//....................................................................................
export function asgieFetchDataSuccess(response) {
    return {
        type: 'ASGIE_FETCH_DATA_SUCCESS',
        response
    };
}

export function asgieHasErrored(bool) {
    return {
        type: 'ASGIE_HAS_ERRORED',
        hasErrored: bool
    };
}

export function asgieIsLoading(bool) {
    return {
        type: 'ASGIE_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function asgie(id) {
    let token = localStorage.token;

    return (dispatch) => {
        dispatch(asgieIsLoading(true));

        axios({
            url:'http://api_mysql.tv4e.pt/api/asgie/'+id,
            method:'get',
            headers:{
            'X-Requested-With': 'XMLHttpRequest',
            'Access-Control-Allow-Origin' : '*',
            'Authorization' : 'Bearer '  + token
            }
        })
        .then((response) => {
            if (response.status != 200) {
                throw Error(response.statusText);
            }
            dispatch(asgieIsLoading(false));
            dispatch(asgieFetchDataSuccess(response.data));
            return response;
        })
        .catch(() => dispatch(asgieHasErrored(true)));
    };
}

//....................................................................................

export function resourcesPostDataSuccess(response) {
    return {
        type: 'RESOURCES_POST_DATA_SUCCESS',
        response
    };
}

export function resourcesPostHasErrored(bool) {
    return {
        type: 'RESOURCES_POST_ERRORED',
        hasErrored: bool
    };
}

export function resourcesPostIsLoading(bool) {
    return {
        type: 'RESOURCES_POST_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function resourcesPost(data) {
    let token = localStorage.token;

    var fd = new FormData();
    fd.append('image', data.image);
    fd.append('asgie', data.asgie);
    fd.append('selected', data.selected);

    return (dispatch) => {
        dispatch(resourcesPostIsLoading(true));

        axios({
            url: 'http://api_mysql.tv4e.pt/api/resources',
            method : 'post',
            headers : {
                'Content-Type' : data.type,
                'Authorization' : 'Bearer '  + token
            },
            data : fd
        })
        .then((response) => {
            if (response.status != 200) {
                throw Error(response.statusText);
            }

            dispatch(resourcesPostIsLoading(false));
            dispatch(resourcesPostDataSuccess(response.data));

            return response;
        })
        .catch(() => dispatch(resourcesPostHasErrored(true)));
    };
}

//....................................................................................

export function deleteSourceDataSuccess(response) {
    return {
        type: 'DELETE_SOURCE_DATA_SUCCESS',
        response
    };
}

export function deleteSourceHasErrored(bool) {
    return {
        type: 'DELETE_SOURCE_HAS_ERRORED',
        hasErrored: bool
    };
}

export function deleteSourceIsLoading(bool) {
    return {
        type: 'DELETE_SOURCE_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function deleteSource(id) {
    let token = localStorage.token;


    return (dispatch) => {
        dispatch(resourcesPostIsLoading(true));

        axios({
            url: 'http://api_mysql.tv4e.pt/api/source/'+id,
            method : 'delete',
            headers : {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization' : 'Bearer '  + token
            }
        })
            .then((response) => {

                dispatch(deleteSourceIsLoading(false));
                dispatch(deleteSourceDataSuccess(response.data));
                dispatch(sources());
                return response;
            })
            .catch(() => dispatch(deleteSourceHasErrored(true)));
    };
}

//....................................................................................

export function deleteResourceSuccess(response) {
    return {
        type: 'DELETE_RESOURCE_SUCCESS',
        response
    };
}

export function deleteResourceHasErrored(bool) {
    return {
        type: 'DELETE_RESOURCE_HAS_ERRORED',
        hasErrored: bool
    };
}

export function deleteResourceIsLoading(bool) {
    return {
        type: 'DELETE_RESOURCE_IS_LOADING',
        isLoading: bool
    };
}

//ACTION CALLS
export function deleteResource(src, asgie_id) {
    let token = localStorage.token;

    return (dispatch) => {
        dispatch(deleteResourceIsLoading(true));

        axios({
            url: 'http://api_mysql.tv4e.pt/api/resource',
            method : 'delete',
            headers : {
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization' : 'Bearer '  + token
            },
            data : {local_path: src}
        })
            .then((response) => {
                dispatch(deleteResourceIsLoading(false));
                dispatch(deleteResourceSuccess(response.data));
                if(asgie_id){dispatch(asgie(asgie_id));}
                return response;
            })
            .catch(() => dispatch(deleteResourceHasErrored(true)));
    };
}
