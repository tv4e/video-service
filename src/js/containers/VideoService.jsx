import React from 'react';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

import VideoService from './../components/videoservice/VideoService';

import * as Actions from './../actions/VideoService'

const mapStateToProps = function(state) {

    return {
        actionData:{
            sources: {
                data:state.sourcesFetchDataSuccess,
                hasErrored: state.sourcesHasErrored,
                isLoading: state.sourcesIsLoading
            },
            asgies: {
                data:state.asgiesFetchDataSuccess,
                hasErrored: state.asgiesHasErrored,
                isLoading: state.asgiesIsLoading
            },
            asgie: {
                data:state.asgieFetchDataSuccess,
                hasErrored: state.asgieHasErrored,
                isLoading: state.asgieIsLoading
            },
            districts: {
                data:state.districtsFetchDataSuccess,
                hasErrored: state.districtsHasErrored,
                isLoading: state.districtsIsLoading
            }
        }

    };
};

const mapDispatchToProps = function(dispatch) {
    return {actionCalls: bindActionCreators(Actions, dispatch)};
};

export default connect(mapStateToProps, mapDispatchToProps)(VideoService);