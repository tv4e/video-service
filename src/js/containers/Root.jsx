import React from 'react';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

import Root from './../components/Root';

import * as Actions from './../actions/Profile'

const mapStateToProps = function(state) {

    return {
    };
};

const mapDispatchToProps = function(dispatch) {
    return {actionCalls: bindActionCreators(Actions, dispatch)};
};

export default connect(mapStateToProps, mapDispatchToProps)(Root);