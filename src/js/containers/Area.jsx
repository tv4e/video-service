import React from 'react';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

import Area from './../components/videoservice/modules/Area';

import * as Actions from './../actions/VideoService'

const mapStateToProps = function(state) {

    return {
        actionData:{
            asgies: {
                data:state.asgiesFetchDataSuccess,
                hasErrored: state.asgiesHasErrored,
                isLoading: state.asgiesIsLoading
            },
            asgie: {
                data:state.asgieFetchDataSuccess,
                hasErrored: state.asgieHasErrored,
                isLoading: state.asgieIsLoading
            }
        }
    };
};

const mapDispatchToProps = function(dispatch) {
    return {actionCalls: bindActionCreators(Actions, dispatch)};
};

export default connect(mapStateToProps, mapDispatchToProps)(Area);