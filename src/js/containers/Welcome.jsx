import React from 'react';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

import Welcome from './../components/welcome/Welcome';

import * as Actions from './../actions/Profile'

const mapStateToProps = function(state) {

    return {
        actionData:{
            login: {
                data:state.loginSuccess,
                hasErrored: state.loginHasErrored,
                isLoading: state.loginIsLoading
            }
        }

    };
};

const mapDispatchToProps = function(dispatch) {
    return {actionCalls: bindActionCreators(Actions, dispatch)};
};

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);