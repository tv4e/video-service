import React from 'react';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

import Form from './../components/forms/FormRedux';

import * as Actions from './../actions/FormRedux'

const mapStateToProps = function(state, ownProps) {
    return {
        formData: state.formData,
        children: ownProps.children
    }
};

const mapDispatchToProps = function(dispatch) {
    return {actionCalls: bindActionCreators(Actions, dispatch)};
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
