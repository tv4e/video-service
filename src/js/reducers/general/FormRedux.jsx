'use strict';
import  _ from 'lodash';

export function formData(state = {}, action) {

    switch (action.type) {
        case 'FORM_DATA':
            let response = _.merge({},state, action.response);
            return response;
            break;
        default:
            return state;
    }
}
