'use strict';

export function sourcesFetchDataSuccess(state = [], action) {
    switch (action.type) {
        case 'SOURCES_FETCH_DATA_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function sourcesIsLoading(state = false, action) {
    switch (action.type) {
        case 'SOURCES_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function sourcesHasErrored(state = false, action) {
    switch (action.type) {
        case 'SOURCES_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}
//**********************************************************************

export function districtsFetchDataSuccess(state = [], action) {
    switch (action.type) {
        case 'DISTRICTS_FETCH_DATA_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function districtsIsLoading(state = false, action) {
    switch (action.type) {
        case 'DISTRICTS_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function districtsHasErrored(state = false, action) {
    switch (action.type) {
        case 'DISTRICTS_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}
//**********************************************************************

export function postSourcesDataSuccess(state = [], action) {
    switch (action.type) {
        case 'POST_SOURCES_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function postSourcesIsLoading(state = false, action) {
    switch (action.type) {
        case 'POST_SOURCES_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function postSourcesHasErrored(state = false, action) {
    switch (action.type) {
        case 'POST_SOURCES_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}
//**********************************************************************
export function asgiesFetchDataSuccess(state = [], action) {
    switch (action.type) {
        case 'ASGIES_FETCH_DATA_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function asgiesIsLoading(state = false, action) {
    switch (action.type) {
        case 'ASGIES_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function asgiesHasErrored(state = false, action) {
    switch (action.type) {
        case 'ASGIES_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}
//**********************************************************************
export function asgieFetchDataSuccess(state = [], action) {
    switch (action.type) {
        case 'ASGIE_FETCH_DATA_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function asgieIsLoading(state = false, action) {
    switch (action.type) {
        case 'ASGIE_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function asgieHasErrored(state = false, action) {
    switch (action.type) {
        case 'ASGIE_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}
//**********************************************************************
export function resourcesPostDataSuccess(state = [], action) {
    switch (action.type) {
        case 'RESOURCES_POST_DATA_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function resourcesPostIsLoading(state = false, action) {
    switch (action.type) {
        case 'RESOURCES_POST_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function resourcesPostHasErrored(state = false, action) {
    switch (action.type) {
        case 'RESOURCES_POST_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}
//**********************************************************************
export function deleteSourceDataSuccess(state = [], action) {
    switch (action.type) {
        case 'DELETE_SOURCE_DATA_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function deleteSourceIsLoading(state = false, action) {
    switch (action.type) {
        case 'DELETE_SOURCE_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function deleteSourceHasErrored(state = false, action) {
    switch (action.type) {
        case 'DELETE_SOURCE_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}

//**********************************************************************
export function deleteResourceSuccess(state = [], action) {
    switch (action.type) {
        case 'DELETE_RESOURCE_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function deleteResourceIsLoading(state = false, action) {
    switch (action.type) {
        case 'DELETE_RESOURCE_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function deleteResourceHasErrored(state = false, action) {
    switch (action.type) {
        case 'DELETE_RESOURCE_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}