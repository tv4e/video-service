'use strict';

export function loginSuccess(state = [], action) {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function loginIsLoading(state = false, action) {
    switch (action.type) {
        case 'LOGIN_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function loginHasErrored(state = false, action) {
    switch (action.type) {
        case 'LOGIN_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}


export function logoutSuccess(state = [], action) {
    switch (action.type) {
        case 'LOGOUT_SUCCESS':
            return action.response;
            break;
        default:
            return state;
    }
}

export function logoutIsLoading(state = false, action) {
    switch (action.type) {
        case 'LOGOUT_IS_LOADING':
            return action.isLoading;
            break;
        default:
            return state;
    }
}

export function logoutHasErrored(state = false, action) {
    switch (action.type) {
        case 'LOGOUT_HAS_ERRORED':
            return action.hasErrored;
            break;
        default:
            return state;
    }
}

