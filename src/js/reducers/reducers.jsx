'use strict';

import * as VideoReducer from './general/VideoService';
import * as Profile from './general/Profile';
import * as FormReducer from './general/FormRedux';
import * as Error from './general/Errors';

import _ from 'lodash';

let reducers = _.extend({},
    VideoReducer,
    Error,
    Profile,
    FormReducer
);

module.exports= reducers;

