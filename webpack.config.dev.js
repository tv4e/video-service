var path = require('path');
var webpack = require('webpack');

var HTMLWebpackPlugin  = require("html-webpack-plugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    devtool:'source-maps',
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
        "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    },
    entry: [
        'webpack-dev-server/client?http://localhost:3000',
        './src/js/index.jsx'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    plugins : [
        new HTMLWebpackPlugin({
            hash:true,
            title: 'videoDashboard_dev',
            filename: 'index.html',
            template: 'index.html',
            favicon: 'src/assets/images/logo/favicon/favicon.ico',
            environment: process.env.NODE_ENV
        }),
        new ExtractTextPlugin("style.css"),
    ],
    module: {
        loaders: [
            //loader for ES6 and babel
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015']
                },
                include: path.join(__dirname, 'src/js')
            },
            //loader for styles
            {
                test: /\.scss$/,
                loaders: ["style", "css", "sass"],
                include: path.join(__dirname, 'src/styles')
            },
            //loader for images
            {
                test: /\.(jpg|png)$/,
                loader: 'url-loader?limit=25000',
                include: path.join(__dirname, 'src/assets/images')
            },
            {
                test: /\.svg$/,
                loader: 'file-loader',
                include: path.join(__dirname, 'src/assets/images')
            },
            //loader for icons
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff" },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            },
            //loader for video
            {
                test: /\.mp4$/,
                loader: 'url?limit=10000&mimetype=video/mp4'
            }
        ]
    },
    postcss: [
        require('autoprefixer')
    ],
    resolve: {
        extensions: ['', '.js', '.jsx']
    }
};